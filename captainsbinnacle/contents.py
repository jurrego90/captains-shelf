#%%
import os
import numpy as np
import pandas as pd

from sklearn.base import TransformerMixin
from sklearn.preprocessing import (OneHotEncoder, StandardScaler, 
                                    PowerTransformer, FunctionTransformer, MinMaxScaler)
from sklearn.decomposition import PCA

"""=====Classes====="""


class train_1param():
    """
    Simple class for training a model varying only one parameter.
    --------------------------------------------------------------------------------------
    estimator: sklearn-like estimator.
    cv = cross validator object.
    param_dict: dictionary containing the name of the parameter and the values evaluate.
    scoring: scorer for evaluating the performance of the model.

    fit inputs:
    x: array with features.
    y: array with target variable.
    """
    def __init__(self, estimator, cv, param_dict, scoring=None):
        self.estimator = estimator
        self.cv = cv
        self.param_dict = param_dict
        self.param_name = list(param_dict.keys())[0]
        self.scoring = scoring
        self.dict_results = {}
        self.dict_results['train'] = [ [],[] ]
        self.dict_results['val'] = [ [],[] ]
        
    def fit(self, x: np.array, y=None)->None:

        for param in self.param_dict[self.param_name]:
            dict_ = {self.param_name:param}
            est = self.estimator(**dict_)
            k_scores_train = []
            k_scores_val = []
            for train_index, val_index in self.cv.split(x, y):
                est.fit(x[train_index])

                if self.scoring==None:
                    k_scores_train.append(est.score(x[train_index]))
                    k_scores_val.append(est.score(x[val_index]))
                else:
                    train_score = self.scoring(est, x[train_index], y[train_index])
                    val_score = self.scoring(est, x[val_index], y[val_index])
                    k_scores_train.append(train_score)
                    k_scores_val.append(val_score)

            mean_k_score_train = np.mean(k_scores_train)
            mean_k_score_val = np.mean(k_scores_val)

            self.dict_results['train'][0].append(mean_k_score_train)
            self.dict_results['train'][1].append(k_scores_train)

            self.dict_results['val'][0].append(mean_k_score_val)
            self.dict_results['val'][1].append(k_scores_val)


class Encoder(TransformerMixin):
    """
    Applies a one-hot encoder to certain columns your dataframe and
    then returns a newdataframe with the columns processed.
    --------------------------------------------------------------------------------------
    columns: List of columns to which the preprocessing will be applied.
    fit inputs:
    df: Dataframe containing the columns that will be processed.
    """
    def __init__(self, columns):
        self.cols = columns

    def fit(self, df, y = None):
        self.onehot_model = OneHotEncoder()
        self.onehot_model.fit(df[[self.cols]])
        self.cols_trans = np.concatenate(self.onehot_model.categories_)
        return self

    def transform(self, df):
        df = df.copy(deep = True)
        data_trans = self.onehot_model.transform(df[self.cols]).toarray()
        df_trans = pd.DataFrame(data_trans, 
                                columns = self.cols_trans)
        df_trans.index = df.index.tolist()                                
        for col in self.cols:
            df.drop(col, axis = 1, inplace = True)
        df = pd.concat([df, df_trans], axis = 1)

        return df


class Pca (TransformerMixin):
    """
    Applies a PCA encoder to certain columns of your dataframe and
    then returns a new dataframe with the processed columns. It takes
    the first n components that explained more than 95% of the variance.
    --------------------------------------------------------------------------------------
    columns: List of columns to which the preprocessing will be applied.
    fit inputs:
    df: Dataframe containing the columns that will be processed.
    """
    def __init__(self, columns, threshold = 0.95):
        self.cols = columns
        self.threshold = threshold

    def fit(self, df, y = None):
        self.pca_model = PCA()
        self.pca_model.fit(df[[self.cols]])
        self.n_components = 0
        explained_variance = self.pca_model.explained_variance_ratio_
        while explained_variance[0:self.n_components].sum()<=0.95:
            self.n_components += 1
        return self

    def transform(self,df):
        df = df.copy()
        data_trans = self.pca_model.transform(df[self.cols])[:,0:self.n_components]
        pca_cols =  ['PCA_{}'.format(i) for i in range(self.n_components)]
        df_pca = pd.DataFrame(data = data_trans, columns = pca_cols)
        for col in self.cols:
            df.drop(col, axis = 1, inplace = True)
        df_pca.index = df.index.tolist()
        df = pd.concat([df, df_pca], axis = 1 )
        return df


class ScalerMinMax(TransformerMixin):
    """
    Applies a Standard Scaler to certain columns of your dataframe and
    then returns a new dataframe with the processed columns.
    --------------------------------------------------------------------------------------
    Init inputs:
    columns: List of columns to which the preprocessing will be applied.

    fit inputs:
    df: Dataframe containing the columns that will be processed.
    """

    def __init__(self, columns):
        self.cols = columns

    def fit(self, df, y = None):
        self.scaler_model = MinMaxScaler()
        self.scaler_model.fit(df[[self.cols]])
        return self

    def transform(self, df, y = None):
        df = df.copy()
        data_scaled = self.scaler_model.transform(df[self.cols])
        df_scaled = pd.DataFrame(data_scaled, columns = self.cols)
        for col in self.cols:
            df.drop(col, axis = 1, inplace = True)
        df_scaled.index = df.index.tolist()
        df = pd.concat([df, df_scaled], axis = 1)
        return df


class StdScaler(TransformerMixin):
    """
    Applies a Standard Scaler to certain columns of your dataframe and
    then returns a new dataframe with the processed columns.
    --------------------------------------------------------------------------------------
    Init inputs:
    columns: List of columns to which the preprocessing will be applied.

    fit inputs:
    df: Dataframe containing the columns that will be processed.
    """

    def __init__(self, columns):
        self.cols = columns

    def fit(self, df, y = None):
        self.scaler_model = StandardScaler()
        self.scaler_model.fit(df[self.cols])
        return self

    def transform(self, df, y = None):
        df = df.copy()
        data_scaled = self.scaler_model.transform(df[self.cols])
        df_scaled = pd.DataFrame(data_scaled, columns = self.cols)
        for col in self.cols:
            df.drop(col, axis = 1, inplace = True)
        df_scaled.index = df.index.tolist()
        df = pd.concat([df, df_scaled], axis = 1)
        return df


class PwrTransformer(TransformerMixin):
    """
    Applies a power transformation to specific columns of your dataframe and
    then returns a new dataframe with the processed columns. The method it applies is
    the yeo-johnson method.
    --------------------------------------------------------------------------------------
    Init inputs:
    columns: List of columns to which the preprocessing will be applied.

    fit inputs:
    df: Dataframe containing the columns that will be processed.
    """

    def __init__(self, columns, method = 'yeo-johnson', standardize = True):
        self.cols = columns
        self.standardize = standardize
        self.method = method

    def fit(self, df, y = None):
        self.power_transformer = PowerTransformer(method = self.method, standardize=self.standardize)
        self.power_transformer.fit(df[self.cols])
        return self

    def transform(self, df, y = None):
        df = df.copy()
        data_transformed = self.power_transformer.transform(df[self.cols])
        df_transformed = pd.DataFrame(data_transformed, columns = self.cols)
        for col in self.cols:
            df.drop(col, axis = 1, inplace = True)
        df_transformed.index = df.index.tolist()
        df = pd.concat([df, df_transformed], axis = 1)
        return df


class FuncTransformer(TransformerMixin):
    """
    Applies a function transformation to specific columns of your dataframe and
    then returns a new dataframe with the processed columns.
    --------------------------------------------------------------------------------------
    Init inputs:
    columns: list of columns to which the preprocessing will be applied.
    func: function to apply to the data.

    fit inputs:
    df: Dataframe containing the columns that will be processed.
    """

    def __init__(self, columns, func):
        self.cols = columns
        self.func = func

    def fit(self, df, y = None):
        self.func_transformer = FunctionTransformer(self.func)
        self.func_transformer.fit(df[self.cols])
        return self

    def transform(self, df, y = None):
        df = df.copy()
        data_transformed = self.func_transformer.transform(df[self.cols])
        df_transformed = pd.DataFrame(data_transformed, columns = self.cols)
        for col in self.cols:
            df.drop(col, axis = 1, inplace = True)
        df_transformed.index = df.index.tolist()
        df = pd.concat([df, df_transformed], axis = 1)
        return df


class Dictionarizer():
    """
    Transforms a dataframe into a multilevel dictionary to be passed as an schema when loading 
    another DataFrame.
    --------------------------------------------------------------------------------------
    class_field: name of the field that stores the information of classes. Classes are dtype o date.
    field_field: name of the column with the field names of the dataframe for which this schema will be defined.
    type_field: name of the column where the specific type of each field is stored.
    """
    def __init__(self, df, class_field = 'CLASS', field_field = 'FIELD', type_field = 'TYPE'):
        self.df = df
        self.class_field = class_field
        self.field_field = field_field
        self.type_field = type_field
        self.dict_final = {}

    def dictionarize_dtypes(self):
        """
        By default, anything that is not DATE is going to be consider
        a DTYPE values.
        """
        condition_dtype = (~self.df[self.class_field].isin(['DATE', 'date']))
        df_dtype = self.df[condition_dtype].reset_index(drop=True).drop(self.class_field, axis = 1)
        key = 'dtype'
        columns_dtype = df_dtype.columns
        dict_dtype = {}
        dict_dtype[key] = df_dtype.set_index(columns_dtype[0]).T.to_dict(orient = 'records')[0]
        return dict_dtype

    def dictionarize_dates(self):
        condition_date = (self.df[self.class_field].isin(['DATE', 'date']))
        df_date = self.df[condition_date].reset_index(drop=True).drop(self.class_field, axis = 1)
        key = 'date'
        dict_date = {}
        dict_date[key] = df_date[self.field_field].values.tolist()
        return dict_date

    def dictionarize_all(self):
        dict_dtype = self.dictionarize_dtypes()
        dict_date = self.dictionarize_dates()
        dict_all = {**dict_date, **dict_dtype}
        return dict_all


"""=====Methods====="""
def log10_(x):
    x_ = x - np.min(x) + 1
    return np.log10(x_)

def gmm_js(gmm_p, gmm_q, i=0, n_samples=10**5):
    """
    Credits: Prof. Danica J. Sutherland, Assistant Professor, UBC Computer Science;
    """
    print(f'js divergence: {i}', end='\r')
    X = gmm_p.sample(n_samples)[0]
    log_p_X = gmm_p.score_samples(X)
    log_q_X = gmm_q.score_samples(X)
    log_mix_X = np.logaddexp(log_p_X, log_q_X)

    Y = gmm_q.sample(n_samples)[0]
    log_p_Y = gmm_p.score_samples(Y)
    log_q_Y = gmm_q.score_samples(Y)
    log_mix_Y = np.logaddexp(log_p_Y, log_q_Y)


#%%
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from sklearn.model_selection import train_test_split, StratifiedKFold, ShuffleSplit
    from sklearn.neighbors import KernelDensity
    

    # Load things
    path_datasets = '..\\..\\..\\Datasets\\raw'
    file_name_test = 'test_for_1param_train.csv'    
    path_test = os.path.join(path_datasets, file_name_test)
    df_test = pd.read_csv(path_test, sep='|').drop('ints', axis=1)
    
    num_bins = 9
    range_ = df_test.max() - df_test.min()
    bin_width = range_.values[0]/num_bins
    bins = np.arange(0, df_test['VENTA_2'].max()+bin_width, bin_width)
    df_test['ints'] = np.digitize(df_test['VENTA_2'], bins)
    cv = StratifiedKFold(n_splits=5)

    # Instantiate the class
    dict_param = {'bandwidth':[0.1, 0.3, 0.5]}
    x_train, x_test, ints_train, ints_test = train_test_split(df_test[['VENTA_2']], 
                                                                df_test[['ints']],                                                              
                                                                test_size=0.2, 
                                                                stratify=df_test['ints'])

    cv = StratifiedKFold(n_splits=5)          
    trainer = train_1param(KernelDensity,cv, dict_param)
    trainer.fit(x_train.values, ints_train.values)
    dict_results = trainer.dict_results

    fig, ax = plt.subplots(figsize=(6,6))
    ax.scatter(dict_param['bandwidth'], np.exp(dict_results['train'][0]))
    ax.scatter(dict_param['bandwidth'], np.exp(dict_results['val'][0]))

    plt.show()

