import pandas as pd
from ..base_transformers import BaseIngestionTransformer


class FuncApplier(BaseIngestionTransformer):
    def __init__(self, **params):
        self.func = params.get('func', None)
        self.params = params.get('func_params', {})

    def change_func(self, **new_params):
        self.__init__(**new_params)

    def transform(self, df: pd.DataFrame)->pd.DataFrame:
        """
        Applies a the function passed to the constructures with df as the main argument
        and the rest passed in params,
        """
        if df.empty or not self.func:
            return df
        return self.func(df, **self.params)      
