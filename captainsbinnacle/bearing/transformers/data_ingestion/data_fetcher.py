from tqdm import tqdm
import awswrangler as wr
import glob
import os
from ..base_transformers import BaseIngestionTransformer


class DataFetcher(BaseIngestionTransformer):
    """
    
    Parameters:
    -----------
    'path': str
            Path where the file(s) are. If 'location' is s3, then is the whole 
            path inside s3 without the systen prefix, s3://.
    'ending_string': str
            String used for filtering the files inside path. Not necessarily 
            and ending string -_-.
    'location': str, {'s3', 'local'}
            Where are you listing the files from.
            s3 -> AWS s3 bucket.
            local -> Local machine
    """
    def __init__(self, **params, )->list[str]:
        self.path = params.get('path', '.')
        self.ending_string = params.get('ending_string', '*.csv')
        self.location = params.get('location', 'local')
    

    def transform(self, data=None):
        # self.full_path = f'{self.path}/*{self.ending_string}' 
        self.full_path = os.path.join(self.path, self.ending_string) # In tests
        return get_objects_extractor(self.location)(self.full_path)



def get_objects_extractor(extractor: str):
    if extractor=='s3':
        return get_s3_objects
    elif extractor=='local':
        return get_local_objects
    else:
        raise ValueError(extractor)


def get_s3_objects(s3_path:str)->list:
    full_path = f's3://{s3_path}'
    print(full_path)
    print('Listing s3 paths...', end='\r')
    objects = wr.s3.list_objects(full_path)
    print('Listing s3 paths...Done', end='\r')
    return [obj for obj in tqdm(objects,  desc='Fetching file paths from s3')]

def get_local_objects(path:str)->list[str]:
    print('Listing local paths...', end='\r')
    result = glob.glob(path)
    print('Listing local paths...Done')
    return result