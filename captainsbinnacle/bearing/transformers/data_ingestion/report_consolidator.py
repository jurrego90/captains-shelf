from typing import Union
from nbformat import read
from tqdm import tqdm
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import numpy as np
import os
from itertools import product
import json

from ..base_transformers import BaseIngestionTransformer


class ReportsConsolidator(BaseIngestionTransformer):
    """
    Takes a list of files and appends them into a single structure. By default, 
    the `transform` method returns a pandas DataFrame, but this condition can 
    be changed by passing "to_dict" as True. In such a case the resulting 
    dataset will be a list of record-oriented dictionaries.

    Parameters:
    -----------

    "consolidator_type": str, {'generic', 'raw', 'processed'} default 'generic'
            If the files are generic csvs, then choose 'generic'. If the
            files are similar to Kline reports choose any of other two.

    "schema": dict, optional default None
            If passed, the consolidator will try to enforce it to every file it includes.

    "to_dict": bool, optional default False
            If True, it will transform the dataset from a dataframe to a record-oriented
            dictionary.

    "data_stage": str, optional {'running', 'historic', 'processed'}
            If the data comes from historic records or from the ongoing
            records.
    """
    def __init__(self, **params)->None:
        self.report_moment = params.get("data_stage", None)
        self.schema = params.get("schema", None)
        self.to_dict = params.get("to_dict", False)
        self.consolidator_type = params.get("consolidator_type", "generic")

    def transform(self, paths: list[str])->Union[pd.DataFrame, list[dict]]:
        """
        Loads all files passed in "path" and concatenates them into a single structure.

        Parameters:
        -----------

        "paths": list[str]
            List of paths where the files are.
        """
        consolidator = self.get_consolidator(self.consolidator_type)
        print('Consolidator Working....')
        df_consolidated_reports = consolidator(paths, self.schema)      
        return self.return_df(df_consolidated_reports, self.to_dict)

    def get_consolidator(self, consolidator_type:str):
        if consolidator_type == 'raw':
            return self.consolidate_csv_raw
        elif consolidator_type =='processed':
            return self.consolidate_csv_processed
        elif consolidator_type =='generic':
            return self.consolidate_generic_csv
        else:
            raise ValueError(consolidator_type)


    def consolidate_csv_processed(self, path_data: str, schema=None)->pd.DataFrame:
        list_dfs = []
        for file in tqdm(path_data, desc='Loading files'):
            file_name = file.split('/')[-1]
            df = pd.read_csv(file)
            unnamed_cols = [col for col in df.columns.tolist() if 'Unnamed' in col]
            df.drop(unnamed_cols, axis=1, inplace=True)
            df = enforce_schema(df, schema)
            df['FILE_NAME'] = file_name
            list_dfs.append(df)

        df_cons = pd.concat(tqdm(list_dfs, desc='Concatenating files'), ignore_index=True)    
        return df_cons    

    def consolidate_csv_raw(self, path_data: str, schema=None)->pd.DataFrame:
        list_dfs = []
        for file in tqdm(path_data, desc='Loading files'):
            file_name = file.split('/')[-1]
            ship_id, report_type = get_id_extractor(self.report_moment)(file_name)
            df = pd.read_csv(file)
            if df.empty:
                continue
            unnamed_cols = [col for col in df.columns.tolist() if 'Unnamed' in col]
            df.drop(unnamed_cols, axis=1, inplace=True)
            df['DATA_STAGE'] = self.report_moment
            df['SHIP_ID'] = ship_id
            df['REPORT_TYPE'] = report_type
            df = enforce_schema(df, schema)
            list_dfs.append(df)

        df_cons = pd.concat(tqdm(list_dfs, desc='Concatenating files'), ignore_index=True)
        return df_cons

    def consolidate_generic_csv(self, path_data:str, schema=None)->pd.DataFrame:
        list_dfs = []
        for file in tqdm(path_data, desc='Loading files'):
            file_name = file.split('/')[-1]
            df = pd.read_csv(file)
            if df.empty:
                continue
            unnamed_cols = [col for col in df.columns.tolist() if 'Unnamed' in col]
            df.drop(unnamed_cols, axis=1, inplace=True)
            df['FILE_NAME'] = file_name
            list_dfs.append(df)

        df_cons = pd.concat(tqdm(list_dfs, desc='Concatenating files'), ignore_index=True)
        return df_cons


class ArrowParquetSaver(BaseIngestionTransformer):
    def __init__(self, **params:dict)->None:
        """
        Parameters:
        -----------
        "partition_cols": list[str]
                List of columns used to partition the original dataset and to
                write (or create) the saving paths.
        "saving_path": str
                Path on top of which the folder tree for storing the partitioned 
                datasets will be created.
        "arrow_params": dict
                Extra parameters passed to `pq.write_to_dataset`.
        """
        self.partition_cols = params.get('partition_cols', None)
        self.saving_path = params.get('saving_path', None)
        self.arrow_params = params.get('arrow_params', {})
        self.existing_data_behavior = params.get('existing_data_behavior', 
                                                    'overwrite_or_ignore')
        self.arrow_schema = params.get('arrow_schema')

    def transform(self, df: pd.DataFrame):
        pq.write_to_dataset(pa.Table.from_pandas(df.copy()), 
                            self.saving_path, 
                            **self.arrow_params)


        

class Saver(BaseIngestionTransformer):
    """
    Saves a dataset partitioned by the columns passed in "partition_cols". If the
    path tree does not exist, it creates it. This class can be used to update existing
    tables or yo overwrite them completely based on the "saving_action" parameter.

    Parameters:
    -----------
    "partition_cols": list[str]
            List of columns used to partition the original dataset and to
            write (or create) the saving paths.

    "partitions_to_drop": list[bool]
            Indicates which elements of the partition_cols are going to be 
            dropped before saving the partitioned tables.
    
    "trip_cols": list[str], default ['SHIP_ID', 'REPORT_TYPE', 'UTC']
            List of columns that combined identify a particular trip. These are used
            for identifying if a particular trip being processes is already in the existing 
            dataset. If this is the case, the rows concerning such a trip will be updated.

    "base_path": str
            Path on top of which the folder tree for storing the partitioned datasets will be
            created.

    "reading_format": str {'csv', 'parquet'}, default 'csv'
            Format of the file to be read in case of updating the file.

    "saving_format": str {'csv', 'parquet'}, default 'csv'
            Final file format.

    "saving_action": str {'update', 'overwrite'}, default 'update'
            Defines if the process will overwrite an existing file or just update it.
    """

    def __init__(self, **params)->None:
        self.partition_cols = params.get("partition_cols")
        self.partitions_to_drop = params.get("partitions_to_drop", [False]*len(self.partition_cols))
        self.trip_cols = params.get("trip_cols", ['SHIP_ID', 'REPORT_TYPE', 'UTC'])
        self.base_path = params.get("base_path")
        self.reading_format = params.get("reading_format", None)
        self.saving_format = params.get("saving_format", "csv")
        self.saving_action = params.get("saving_action", 'update')

        self.saving_procedure = self.get_saving_procedure(self.saving_action)
        self.check_input_variables(self.reading_format)
        self.cols_to_drop = np.array(self.partition_cols)[self.partitions_to_drop].tolist()

    def check_input_variables(self, 
                              reading_format):
        if self.saving_action == 'update' and not reading_format:
            raise TypeError(reading_format)
        
    def transform(self, data: list[dict])->list[dict]:
        df = pd.DataFrame(data)
        combos = list(df[self.partition_cols].drop_duplicates().to_records(index=False))
        queries = [dict(zip(self.partition_cols, combo)) for combo in combos]
        for query in tqdm(queries, desc='Saving files'):
            partial_path = os.path.join(self.base_path, *[element+"="+str(query[element]) for element in query])
            file_name = '_'.join(query.values())+f'.{self.saving_format}'
            self.saving_procedure(df.loc[(df[list(query)]==pd.Series(query)).all(axis=1)], partial_path, file_name)

    def get_saving_procedure(self, saving_action: str):
        if saving_action=='update':
            return self.save_update
        if saving_action=='overwrite':
            return self.save_overwrite
        else:
            raise ValueError(saving_action)
       
    def save_update(self, df, path, file_name):
        full_path = os.path.join(path, file_name)
        if not os.path.exists(path):
            os.makedirs(path)
        elif os.path.exists(full_path):
            # df_base = pd.read_csv(full_path)
            df_base = self.read_df(full_path, self.reading_format)
            schema_df_base = convert_pd_schema(df_base.dtypes.to_dict())
            df = enforce_schema(df, schema_df_base)
            condition_to_replace = df_base[self.trip_cols].isin(df[self.trip_cols]).all(axis=1)
            if condition_to_replace.any():
                df_base.loc[condition_to_replace] = df.copy()
                df = df_base.copy()
            else:
                df = pd.concat([df, df_base], ignore_index=True)
            del df_base
        df = df.drop(self.cols_to_drop, axis=1)
        self.save_df(df, os.path.join(path, file_name), self.saving_format)

    def save_overwrite(self, df:pd.DataFrame, path:str, file_name:str):
        full_path = os.path.join(path, file_name)
        if not os.path.exists(path):
            os.makedirs(path)
        # df.to_csv(full_path, index=False)
        df = df.drop(self.cols_to_drop, axis=1)
        self.save_df(df, full_path, self.saving_format)

    def read_df(self, path:str, format:str, **kwargs: dict):
        if format=='csv':
            return pd.read_csv(path, **kwargs)
        elif format=='parquet':
            return pd.read_parquet(path, **kwargs)
        else:
            raise ValueError(format)

    def save_df(self, df:pd.DataFrame, path:str, format: str, **kwargs:dict):
        if format=='csv':
            df.to_csv(path, index=False, **kwargs)
        elif format=='parquet':
            df.to_parquet(path, index=False, **kwargs)
        else:
            raise ValueError(format) 



class JsonSaver(BaseIngestionTransformer):
    def save_json(self, dictionary, path, file_name):
        full_path = os.path.join(path, file_name)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(full_path, 'w') as f:
            json.dump(dictionary, f)
        f.close()
        


"""--------------------------------- Methods -----------------------------------------------"""



def convert_pd_schema(pd_schema:dict):
    dict_dtypes = {'object':'str', 'float64': 'float', 'int64':'int', 'bool':'bool'}
    return {key:dict_dtypes[pd_schema[key].name] for key in pd_schema}


def _extract_id_report_running(file:str)->str:
    split_file = file.split('_')
    ship_id = split_file[0]
    report_type = split_file[1]
    return ship_id, report_type

def _extract_id_report_historic(file:str)->str:
    split_file = file.split('_')
    ship_id = split_file[2]
    report_type = split_file[1]
    return ship_id, report_type

def enforce_schema(df:pd.DataFrame, schema:dict)->pd.DataFrame:
    if not schema:
        return df
    
    cols_df = df.columns.tolist()
    for col in schema:
        if col not in cols_df:
            df[col] = np.nan
        if schema[col] != 'str' and schema[col] != str:
            df[col] = pd.to_numeric(df[col], errors='coerce')

        df[col] = df[col].astype(eval_func(schema[col]))
    df = df[schema.keys()]
    return df

def eval_func(x):
    if type(x)==str:
        return eval(x)
    else:
        return x


def get_id_extractor(extractor: str):
    """
    Expected name format for Running files = SHIPID_REPORTTYPE_*_*_full_component_analysis.csv
    Expected name format for Historic files = *_REPORTTYPE_SHIPID_*_*_weather.csv
    Expected name format for Processed files = SHIPID_REPORT_TYPE_*.csv
    """
    if extractor=='historic':
        return _extract_id_report_historic
    elif extractor=='running':
        return _extract_id_report_running
    elif extractor=='processed':
        return _extract_id_report_running
    else:
        raise ValueError(format)