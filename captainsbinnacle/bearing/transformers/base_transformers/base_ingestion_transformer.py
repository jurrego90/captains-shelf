import pandas as pd
from .base_transformer import  BaseTransformer


class BaseIngestionTransformer(BaseTransformer):
    def fit(self, data=None, labels=None)->None:
        return self

    def return_df(self, df:pd.DataFrame, to_dict=False):
        """
        If to_dict=True, it transforms the input dataframe to a record-oriented
        dictionary.
        """
        if to_dict:
            print('Converting to dict...', end='\r')
            df = df.to_dict('records')
            print('Converting to dict...Done')
        return df

    def transform(self, data=None):
        """
        Transforms data according to class specific needs.
        """
        return data

    def fit_transform(self, data=None, labels=None):
        self.fit(data)
        return self.transform(data)