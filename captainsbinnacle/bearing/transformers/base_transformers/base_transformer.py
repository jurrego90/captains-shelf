
from sklearn.base import TransformerMixin

class BaseTransformer(TransformerMixin):
    """Basic Transformer."""

    def __repr__(self):
        return self.__class__.__name__

    @property
    def name(self):
        """Shorthand for the classname."""
        return self.__class__.__name__

    def transform_single(self, item):
        """Identity transformer for single item."""
        return item

    def transform(self, data):
        """Transform messages into tokens."""
        return list(map(self.transform_single, data))

    def fit(self, data, labels):
        """Identity fit."""
        return self

    def _differentiate_data(self, data):
        diff_data = data.diff()
        diff_data[:1] = diff_data[:1].fillna(0)
        diff_data.columns = [str(col) + '_delta' for col in diff_data.columns]
        return diff_data

    def _save(self):
        return {}

    @staticmethod
    def transform_to_dict(data, result, label):
        for i, point in enumerate(data):
            point[label] = result[i]
        return data

    @staticmethod
    def get_features(data, features):
        data = [[one_data[fld] for fld in features] for one_data in data]
        return data

    @staticmethod
    def get_label(data, label, new_label):
        if new_label:
            y = BaseTransformer.get_features(data, [new_label])
        else:
            y = label
        return y