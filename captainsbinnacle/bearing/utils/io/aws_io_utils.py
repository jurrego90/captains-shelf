import s3fs
import boto3
import json


def read_json_s3(s3_uri:str):
    """
    Reads a json file from s3
    """
    bucket, key = split_s3_paths(s3_uri)
    s3 = boto3.resource('s3')
    content_object = s3.Object(bucket, key)
    file_content = content_object.get()['Body'].read().decode('utf-8')
    return json.loads(file_content)


    
def glob_s3(s3_uri: str) -> list[str]:
    """
    Lists elements inside a s3 path that match the
    string passed in s3_uri
    """
    s3 = s3fs.S3FileSystem()
    return s3.glob(s3_uri)

def split_s3_paths(s3_uri: str):
    bucket, key = s3_uri.replace('s3://', '').split('/', 1)
    return bucket, key
